package com.konduto;

import com.konduto.module1.Module1;
import org.junit.Assert;
import org.junit.Test;

public class TestModule1 {

    @Test
    public void testGetters() {
        Module1 module1 = new Module1("John", "Doe");
        Assert.assertEquals("John", module1.getName());
        Assert.assertEquals("Doe", module1.getSurname());
    }
}
