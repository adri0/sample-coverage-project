package com.konduto.module1;

/**
 * Hello world!
 *
 */
public class Module1 {

    private String name;
    private String surname;

    public Module1(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public Module1 withName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public Module1 withSurname(String surname) {
        this.surname = surname;
        return this;
    }
}
