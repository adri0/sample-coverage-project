package com.konduto.module2;

public class Module2 {

    private int length;
    private int height;
    private int weight;

    public Module2(int length, int height, int weight) {
        this.length = length;
        this.height = height;
        this.weight = weight;
    }

    public int getLength() {
        return length;
    }

    public Module2 withLength(int length) {
        this.length = length;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public Module2 withHeight(int height) {
        this.height = height;
        return this;
    }

    public int getWeight() {
        return weight;
    }

    public Module2 withWeight(int weight) {
        this.weight = weight;
        return this;
    }
}
