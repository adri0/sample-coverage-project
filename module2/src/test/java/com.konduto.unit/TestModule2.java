package com.konduto.unit;

import com.konduto.module2.Module2;
import org.junit.Assert;
import org.junit.Test;

public class TestModule2 {

    @Test
    public void testGetters() {
        Module2 module2 = new Module2(1, 2, 3);
        Assert.assertEquals(2, module2.getHeight());
        Assert.assertEquals(1, module2.getLength());
        Assert.assertEquals(3, module2.getWeight());
    }
}
